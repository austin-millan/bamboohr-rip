package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/austin-millan/surf"
	"github.com/austin-millan/surf/agent"
	"github.com/austin-millan/surf/browser"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"os"
	"sort"
	"strings"
	"time"
	"github.com/robfig/cron/v3"
)

const (
	layoutISO = "01/02/2006"
)

var (
	cronExp string
	username              string
	password              string
	company               string
	removeDate            string
	filepath              string
	showDates             bool
	pull                  bool
	hiredFired            bool
	loginURLFormatter     = "https://%s.bamboohr.com/login.php"
	employeesURLFormatter = "https://%s.bamboohr.com/employee_directory/ajax/get_directory_info"
)

const defaultFilepath = "employee_archive.json"

type timeSlice []time.Time

type client struct {
	companyName string
	username    string
	password    string
	filepath    string
	httpClient  *http.Client
	b           *browser.Browser
}

// EmployeeDirectory contains all employees data scraped from BambooHR
type EmployeeDirectory struct {
	Employees []struct {
		FirstName     string        `json:"firstName"`
		LastName      string        `json:"lastName"`
		PrefName      string        `json:"prefName"`
		JobTitle      string        `json:"jobTitle"`
		Department    string        `json:"department"`
		DirectReports []interface{} `json:"directReports"`
		Division      interface{}   `json:"division"`
		Email         string        `json:"email"`
		Location      string        `json:"location"`
		ImgURL        string        `json:"imgUrl"`
		ReportsTo     interface{}   `json:"reportsTo"`
		WorkPhone     string        `json:"workPhone"`
		Ext           string        `json:"ext"`
		PersonalPhone interface{}   `json:"personalPhone"`
		ID            int           `json:"id"`
		LinkedInURL   interface{}   `json:"linkedInUrl"`
		TwitterURL    interface{}   `json:"twitterUrl"`
		FacebookURL   interface{}   `json:"facebookUrl"`
		InstagramURL  interface{}   `json:"instagramUrl"`
		TimeOff       interface{}   `json:"timeOff"`
		TimeOffIcon   interface{}   `json:"timeOffIcon"`
		Skype         interface{}   `json:"skype"`
		Pinterest     interface{}   `json:"pinterest"`
	} `json:"employees"`
}

// DateEmployeeDirectoryArchive is an internal representation of a dated archive containing employee data
type DateEmployeeDirectoryArchive struct {
	Data map[string]EmployeeDirectory `json:"Data"`
}

// NewArchive loads a new archive from file
func NewArchive(filepath string) (archive *DateEmployeeDirectoryArchive, err error) {
	archive = &DateEmployeeDirectoryArchive{
		Data: make(map[string]EmployeeDirectory, 0),
	}
	jsonFile, err := os.Open(filepath)
	if err != nil {
		if os.IsNotExist(err) {
			if _, err = os.Create(filepath); err != nil {
				return
			}
		} else {
			return
		}
	} else {
		defer jsonFile.Close()
	}
	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return archive, err
	}
	if len(byteValue) > 0 {
		if err = json.Unmarshal(byteValue, archive); err != nil {
			return archive, nil
		}
		return archive, nil
	}
	return archive, nil
}

// Save exports the current to file overwriting contents
func (archive *DateEmployeeDirectoryArchive) Save(filepath string) error {
	var err error
	var d []byte
	if _, err := os.Stat(filepath); err != nil {
		if os.IsNotExist(err) {
			if _, err = os.Create(filepath); err != nil {
				return err
			}
		} else {
			return err // unhandled error
		}
	}
	if d, err = json.Marshal(archive); err != nil {
		return err
	}
	return ioutil.WriteFile(filepath, d, 0755)
}

// Dates is a utility function on the archive that gathers all the dates from the archive.Data map keys.
func (archive *DateEmployeeDirectoryArchive) Dates() []time.Time {
	allDates := make([]time.Time, 0)
	for date := range archive.Data {
		if t, err := time.Parse(layoutISO, date); err != nil {
			log.Print("got error parsing date: %s: %s", date, err.Error())
			continue
		} else {
			allDates = append(allDates, t)
		}
	}
	return allDates
}

// Insert is a helper function for inserting a new employee directory into an in-memory archive
func (archive *DateEmployeeDirectoryArchive) Insert(date string, data EmployeeDirectory, override bool) error {
	k, maxK := 0, 24
	nextDateStr := func(d string, n int) string {
		return fmt.Sprintf("%s.%d", d, n) // ex: 03/30/2030.1, 03/30/2030.2
	}
	if archive.Data == nil {
		archive.Data = make(map[string]EmployeeDirectory, 0)
	}
	for {
		if k > maxK {
			return fmt.Errorf("why poll this site 24x/day")
		}
		if k > 0 { // handles override==false && date already in archive
			if _, ok := archive.Data[nextDateStr(date, k)]; ok {
				k++
				continue
			} else {
				archive.Data[nextDateStr(date, k)] = data
				return nil
			}
		} else { // handles override==true || date not already in archive
			if _, ok := archive.Data[date]; ok {
				if override {
					archive.Data[date] = data
					return nil
				}
				k++
				continue
			}
			archive.Data[date] = data
			return nil
		}
	}
}

func newClient(companyName, username, password, filepath string) (cli client, err error) {
	if len(companyName) == 0 {
		if val := os.Getenv("BAMBOOHR_COMPANY"); len(val) == 0 {
			return cli, fmt.Errorf("missing: companyName")
		} else {
			companyName = val
		}
	}
	if len(username) == 0 {
		if val := os.Getenv("BAMBOOHR_USER"); len(val) == 0 {
			return cli, fmt.Errorf("missing: username")
		} else {
			username = val
		}
	}
	if len(password) == 0 {
		if val := os.Getenv("BAMBOOHR_PASS"); len(val) == 0 {
			return cli, fmt.Errorf("missing: password")
		} else {
			password = val
		}
	}
	if len(filepath) == 0 {
		filepath = defaultFilepath
		log.Printf("using default filepath: %s", filepath)
	}
	jar, err := cookiejar.New(&cookiejar.Options{
		PublicSuffixList: nil,
	})
	if err != nil {
		log.Fatal(err)
	}
	b := surf.NewBrowser()
	b.SetUserAgent(agent.Firefox())
	b.SetCookieJar(jar)
	return client{
		companyName: companyName,
		username:    username,
		password:    password,
		filepath:    filepath,
		//httpClient: &http.Client{Jar: jar},
		b: b,
	}, nil

}

// Do is the main function for loading a new archive, scraping data from BambooHR, and saving the data to file.
func (c client) Do() {
	var err error
	var employeeDirectory *EmployeeDirectory
	var archive *DateEmployeeDirectoryArchive
	if archive, err = NewArchive(c.filepath); err != nil {
		log.Fatal(err)
	} else if archive == nil {
		log.Fatal("Archive is null")
	}
	if err = c.login(); err != nil {
		log.Fatal(err)
	}
	if employeeDirectory, err = c.pullEmployees(); err != nil {
		log.Fatal(err)
	}
	today := time.Now().Format(layoutISO)
	if err = archive.Insert(today, *employeeDirectory, true); err != nil {
		log.Fatal(err)
	}
	if err = archive.Save(c.filepath); err != nil {
		log.Fatal(err)
	}
}

func (c client) login() error {
	var err error
	var loginForm browser.Submittable
	if err = c.b.Open(fmt.Sprintf(loginURLFormatter, c.companyName)); err != nil {
		return err
	}
	forms := c.b.Forms()
	for _, form := range forms {
		if form != nil {
			loginForm = form
		}
	}
	if loginForm == nil {
		return fmt.Errorf("no login form found")
	}
	loginForm.Input("username", c.username)
	loginForm.Input("password", c.password)
	if err = loginForm.Submit(); err != nil {
		return err
	}
	return nil
}

func (c client) pullEmployees() (*EmployeeDirectory, error) {
	var err error
	if err = c.b.Open(fmt.Sprintf(employeesURLFormatter, c.companyName)); err != nil {
		return nil, err
	}
	employeeDirectory := EmployeeDirectory{}
	if err = json.Unmarshal([]byte(c.b.Text()), &employeeDirectory); err != nil {
		return nil, err
	}
	return &employeeDirectory, nil
}

// prune is a utility function for when you want to reduce space by removing duplicate EmployeeDirectories for a
// archive's specific keeping only the latest.
// TODO: implement
// Example:
//   - 01/03/2030 <- old, remove
//   - 01/03/2030.1 <- latest, keep, rename to: 01/03/2030
func (c client) prune() error {
	var archive *DateEmployeeDirectoryArchive
	var err error
	log.Print("prune() not implemented yet")
	if archive, err = NewArchive(c.filepath); err != nil {
		log.Fatal("Can't create new archive" + err.Error())
	} else if archive == nil {
		log.Fatal("Archive is null")
	}
	duplicates := make(map[string]bool)
	for date := range archive.Data {
		if strings.Contains(date, ".") {
			// duplicate
			duplicates[date] = true
		}
	}
	for date := range duplicates {
		log.Printf("Found duplicates: %s", date)
	}

	return nil
}

func (c *client) ShowDates() {
	var archive *DateEmployeeDirectoryArchive
	var err error
	var i int
	if archive, err = NewArchive(c.filepath); err != nil {
		log.Fatal(err)
	} else if archive == nil {
		log.Fatal("Archive is null")
	}
	sortedDates := make(timeSlice, len(archive.Data))
	for date := range archive.Data {
		if t, err := time.Parse(layoutISO, date); err != nil {
			log.Printf("Unable to parse date: %s", date)
			continue
		} else {
			sortedDates[i] = t
			i++
		}
	}
	sort.Sort(sortedDates)
	for _, date := range sortedDates {
		log.Print(date.Format(layoutISO))
	}
}

func (c *client) RemoveDate(dateStrToRemove string) {
	var archive *DateEmployeeDirectoryArchive
	var err error
	if archive, err = NewArchive(c.filepath); err != nil {
		log.Fatal(err)
	} else if archive == nil {
		log.Fatal("Archive is null")
	}
	if dateToRemove, err := time.Parse(layoutISO, dateStrToRemove); err != nil {
		log.Print("Expected date format: " + layoutISO)
		log.Fatal("Unable to parse provided date: " + dateStrToRemove)
	} else {
		toRemove := dateToRemove.Format(layoutISO)
		if _, ok := archive.Data[toRemove]; !ok {
			log.Fatal("Date not found in archive")
		} else {
			delete(archive.Data, toRemove)
			if err = archive.Save(c.filepath); err != nil {
				log.Fatal(err)
			}
		}
	}

}

// HiredFired over time (display from file who is hired/fired over time)
func (c client) HiredFired() {
	var err error
	var i int
	var archive *DateEmployeeDirectoryArchive
	if archive, err = NewArchive(c.filepath); err != nil {
		log.Fatal(err)
	} else if archive == nil {
		log.Fatal("Archive is null")
	}
	sortedDates := make(timeSlice, len(archive.Data))
	for date := range archive.Data {
		if t, err := time.Parse(layoutISO, date); err != nil {
			log.Printf("Unable to parse date: %s", date)
			continue
		} else {
			sortedDates[i] = t
			i++
		}
	}
	sort.Sort(sortedDates)
	employeeIDName := make(map[int]string)
	employeePrev := make(map[int]bool, 0)

	// initialize
	if len(sortedDates) > 0 {
		firstDayData := archive.Data[sortedDates[0].String()]
		for _, employee := range firstDayData.Employees {
			employeeIDName[employee.ID] = fmt.Sprintf("%s %s ([%s] %s: %s)", employee.FirstName, employee.LastName, employee.Location, employee.Department, employee.JobTitle)
			employeePrev[employee.ID] = true
		}
	}
	sortedDates = sortedDates[1:]
	// print transitions
	for j, date := range sortedDates {
		employeeCurr := make(map[int]bool, 0)
		if val, ok := archive.Data[date.Format(layoutISO)]; !ok {
			log.Print("Odd, somehow the data got removed in the middle of processing, returning early")
			continue
		} else {
			for _, employee := range val.Employees {
				// add new ID:name if needed
				if _, ok := employeeIDName[employee.ID]; !ok {
					employeeIDName[employee.ID] = fmt.Sprintf("%s %s ([%s] %s: %s)", employee.FirstName, employee.LastName, employee.Location, employee.Department, employee.JobTitle)
				}
				// add employees to next day map
				employeeCurr[employee.ID] = true
			}
		}

		// compare left (prev) and right (curr) maps
		// missing values in curr (existing in prev) means someone is fired
		for k1 := range employeePrev {
			if _, ok := employeeCurr[k1]; !ok {
				// someone is fired
				log.Printf("%s: RIP: %s", date, employeeIDName[k1])
			}
		}
		// compare right (curr) and left (prev) maps
		// missing values in prev (existing in curr) means someone got hired
		for k2 := range employeeCurr {
			if _, ok := employeePrev[k2]; !ok {
				// someone is hired
				if j > 0 { // skip genesis
					log.Printf("%s: WELCOME: %s", date, employeeIDName[k2])
				}
			}
		}
		employeePrev = employeeCurr
	}
}

// Less implements the Sort interface function for Less
func (s timeSlice) Less(i, j int) bool { return s[i].Before(s[j]) }

// Swap implements the Sort interface function for Swap
func (s timeSlice) Swap(i, j int) { s[i], s[j] = s[j], s[i] }

// Len implements the Sort interface function for Len
func (s timeSlice) Len() int { return len(s) }

func init() {
	// note if you provide parameters via CLI and environment variables, CLI takes precedence
	flag.StringVar(&cronExp, "cron", "", "internal cron scheduler (e.g. \"0 * * * *\")")
	flag.StringVar(&username, "email", "", "your username username/email")
	flag.StringVar(&password, "password", "", "your bamboohr password")
	flag.StringVar(&company, "company", "", "your bamboohr company")
	flag.StringVar(&filepath, "filepath", defaultFilepath, "where you want your archive")
	flag.BoolVar(&showDates, "dates", false, "whether you want to print the dates you have in your archive")
	flag.BoolVar(&pull, "pull", false, "whether you want to pull employee data from BambooHR")
	flag.BoolVar(&hiredFired, "hiredFired", false, "whether you want to show who's been hired/fired over time")
	flag.StringVar(&removeDate, "removeDate", "", "remove employee data from this date")
}

func block() {
	c := make(chan struct{})
	<-c
}

func main() {
	var err error
	var cli client
	var c *cron.Cron
	flag.Parse()
	if len(cronExp) > 0 {
		c = cron.New()
	}
	if cli, err = newClient(company, username, password, filepath); err != nil {
		log.Fatalf("error initializing client: %s", err.Error())
	}
	if pull {
		cli.Do()
		if len(cronExp) > 0 {
			if c == nil {
				log.Fatal("cron scheduler is empty")
			}
			if _, err = c.AddFunc(cronExp, cli.Do); err != nil {
				log.Fatal(err)
			}
		}
	}
	if showDates {
		cli.ShowDates()
		if len(cronExp) > 0 {
			if c == nil {
				log.Fatal("cron scheduler is empty")
			}
			if _, err = c.AddFunc(cronExp, cli.ShowDates); err != nil {
				log.Fatal(err)
			}
		}
	}
	if hiredFired {
		cli.HiredFired()
		if len(cronExp) > 0 {
			if c == nil {
				log.Fatal("cron scheduler is empty")
			}
			if _, err = c.AddFunc(cronExp, cli.HiredFired); err != nil {
				log.Fatal(err)
			}
		}
	}
	if len(removeDate) > 0 {
		cli.RemoveDate(removeDate)
	}
	if len(cronExp) > 0 {
		block()
	}
	return
}
