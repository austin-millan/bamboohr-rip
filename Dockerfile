## We specify the base image we need for our
## go application
FROM golang:1.16.3-alpine
WORKDIR /app
ADD . .
RUN go build -o main .
ENTRYPOINT ["/app/main", "--help"]
