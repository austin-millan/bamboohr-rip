module gitlab.com/austin-millan/bamboohr-rip

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.6.1 // indirect
	github.com/austin-millan/surf v1.0.1-0.20210310222715-580da4943d32
	github.com/robfig/cron/v3 v3.0.0 // indirect
)
