# bamboohr-rip

Track your company's growth over time on BambooHR with bamboohr-rip.

## About

- Authenticate to BambooHR,
- Scrape employee listing data into a simple text JSON file,
- Provides helper CLI utilities to manipulate simple text JSON file,
- Does not need direct API

## Install

### Standalone

```bash
go install gitlab.com/austin-millan/bamboohr-rip
```

### Docker

```bash
git clone gitlab.com/austin-millan/bamboohr-rip
cd bamboohr-rip
docker build -t bamboohr-rip:latest .
```

## Usage

### Standalone

```bash
bamboohr-rip -h
Usage of ./bamboohr-rip:
  -company string
        your bamboohr company
  -cron string
        internal cron scheduler (e.g. "0 * * * *")
  -dates
        whether you want to print the dates you have in your archive
  -email string
        your username username/email
  -filepath string
        where you want your archive (default "employee_archive.json")
  -hiredFired
        whether you want to show who's been hired/fired over time
  -password string
        your bamboohr password
  -pull
        whether you want to pull employee data from BambooHR
  -removeDate string
        remove employee data from this date
```

### Docker

Get usage:

```bash
docker run --entrypoint "/app/main" bamboohr-rip:latest --help
```

#### Configure

You can use a dot file when running this container, just copy the example file
and update it:

```bash
cp .env-example .env
# set your env vars
```

You can run the program with a scheduler:

```bash
	mkdir -p data/
	@docker run \
        --detach \
        --volume data:/app:rw \
        --env-file .env \
        --entrypoint "/app/main" \
        bamboohr-rip:latest \
        --hiredFired --pull --cron="0 * * * *"
```
