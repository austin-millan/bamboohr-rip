.EXPORT_ALL_VARIABLES:

GOOS=linux
GOARCH=amd64

build-windows:
	GO111MODULES=on GOOS=windows CGO_ENABLED=0 go build -o bamboohr-rip.exe .

build-linux:
	GO111MODULES=on GOOS=linux GOARCH=amd64 go build -o bamboohr-rip .

docker-build:
	@docker build -t bamboohr-rip:latest .

docker-run-help: docker-build
	mkdir -p data/
	@docker run \
		--entrypoint "/app/main" \
		bamboohr-rip:latest \
		--help

docker-run-cron:
	mkdir -p data/
	@docker run \
		--detach \
		--volume data:/app:rw \
		--env-file .env\
		--entrypoint "/app/main" \
		bamboohr-rip:latest \
		--hiredFired --pull --cron="0 * * * *"